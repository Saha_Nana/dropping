/**
 * jQuery of the whole SQUARE mega menu plugin
 * for the front-end.
 *
 * @version 1.0.0
 */

jQuery.noConflict();

jQuery(window).load(function(){
	
	/****************************
	 * VARIABLES INITIALIZATION *
	 ***************************/
	var siteWidth = 960,
		espace_publicColor = '#049EC1',
		espace_proColor = '#F37D13';

	/****************************
	 *  PROCESSING THE JQUERY!  *
	 ***************************/
	var bodyWidth = jQuery('body').width(),
		bodyCenter = parseFloat(bodyWidth / 2),
		siteContainerOffsetLeft = parseFloat(bodyCenter - (siteWidth/2)),
		siteContainerOffsetRight = parseFloat(bodyCenter + (siteWidth/2));
	
	// when mouse enter the 'li.menu-item' area
	jQuery('li.menu-item').mouseenter(function(){
		
		var $this = jQuery(this);
		$this.children('ul.sub-menu').stop(true, true).fadeIn('fast');
		
	}).mouseleave(function(){
		
		var $this = jQuery(this);
		$this.children('ul.sub-menu').stop(true, true).fadeOut('fast');
		
	});
	
	jQuery('.menu').children('li.menu-item').each(function(){
		
		// if menu has sub menus
		if(jQuery(this).children('.sub-menu').length){
		
			// if menu is not full width
			if(!jQuery(this).hasClass('square-mega-menu-fullwidth')){
			
				var submenuWidth = jQuery(this).children('.sub-menu').outerWidth(),
					submenuOffset = jQuery(this).children('.sub-menu').offset(),
					submenuOffsetRight = submenuOffset.left + submenuWidth;
				
				if(submenuWidth < siteWidth){
					
					// jQuery(this).children('.sub-menu').children('.sub-menu').css({
					
						// 'width' : '230px',
						// 'display' : 'inline-block'
					
					// });
					
					jQuery(this).children('.sub-menu').children('.sub-menu:not(:nth-child(4)n):not(:last-child)').css({
					
						'margin-right' : '10px'
					
					});
					
					var count = jQuery(this).children('.sub-menu').children('.sub-menu').length,
						width = jQuery(this).children('.sub-menu').children('.sub-menu').outerWidth(),
						padding = jQuery(this).children('.sub-menu').css('padding-left').replace('px','') * 2,
						border = jQuery(this).children('.sub-menu').css('border-left-width').replace('px','') * 2;
						
					if(count > 4) count = 4;
						
					var marginRight = 10 * (count - 1);
						
					jQuery(this).children('.sub-menu').css({
					
						'width' : ((count * parseFloat(width)) + parseFloat(padding) + parseFloat(border) + parseFloat(marginRight)) + 'px'
					
					});
					
					var submenuWidth = jQuery(this).children('.sub-menu').outerWidth(),
						submenuOffset = jQuery(this).children('.sub-menu').offset(),
						submenuOffsetRight = submenuOffset.left + submenuWidth;
					
					if(submenuOffsetRight > siteContainerOffsetRight){
					
						jQuery(this).children('.sub-menu').css({
						
							'margin-left' : (siteContainerOffsetRight - submenuOffsetRight) + 'px'
						
						});
					
					}
				
				}
				
			// if menu is full width
			} else {
				
				var $menu_offset = jQuery('#mainmenu').children('.wrapper:first-child').offset(),
					$this_offset = jQuery(this).offset(),
					$offset_left = $this_offset.left - $menu_offset.left;
				
				jQuery(this).children('.sub-menu').css({
					
					'width' : siteWidth,
					'left' : -$offset_left + 'px'
					
				}).children('.sub-menu').css({
				
					'width' : '299px'
				
				});
				
				jQuery(this).children('.sub-menu').children('.sub-menu:not(:nth-child(3n)):not(:last-child)').css({
					
					'margin-right' : '15.75px'
				
				});
				
				jQuery(this).children('.sub-menu').children('.sub-menu:nth-child(3n):last-child').css({
					
					'border' : '0',
					'padding' : '0'
					
				}).children('li').css({
					
					'padding' : '0'
					
				});
				
			}
			
			// titles in menu should all have same height
			var _height = 0;
			
			jQuery(this).children('.sub-menu').children('.sub-menu').find('.square-mega-menu-title').each(function(){
			
				var _thisHeight = jQuery(this).height();
			
				if(_thisHeight > _height){ _height = _thisHeight; }
			
			});
			
			jQuery(this).children('.sub-menu').children('.sub-menu').find('.square-mega-menu-title').height(_height);
			
			// include a bullet in front of link
			// jQuery(this).children('.sub-menu').children('.sub-menu').children('li').children('a').before('<span class="square-mega-menu-bullet">&#8227;</span>');
			
			// include an arrow on top of menu
			jQuery(this).children('.sub-menu').prepend('<div class="square-mega-menu-top-arrow"></div>');
			
			var $liOffset = jQuery(this).offset(),
				$thisWidth = jQuery(this).outerWidth(),
				$topArrowOffset = jQuery(this).find('.square-mega-menu-top-arrow').offset(),
				$topArrowWidth = jQuery(this).find('.square-mega-menu-top-arrow').outerWidth();
			
			// if top arrow is not +20px on right of the li menu.item (for instance)
			if($topArrowOffset.left <= ($liOffset.left + ($thisWidth / 2) - ($topArrowWidth / 2))){
				
				var $difference = $liOffset.left - $topArrowOffset.left + ($thisWidth / 2) - ($topArrowWidth / 2);
				
				jQuery(this).find('.square-mega-menu-top-arrow').css({
				
					'margin-left' : $difference + 'px'
				
				});
			
			}
			
		}
	
	});
	
	// Initialization
	jQuery('li.menu-item').children('.sub-menu').css('visibility','visible').hide();
	
});