
jQuery(document).ready(function ($) {

    // remove &nbsp
    $("p,li").each(function() {
      var $this = $(this);
      $this.html($this.html().replace(/&nbsp;/g, ' '));
    });

    /**
     * Radio buttons sub titles
     */
    if( $('body').hasClass('page-template-page-suppliers-signup') && $('.checkboxes').length ) {
        $('.form-sub-free-text').each(function() {
           $(this).insertAfter( $(this).prev('.input').find('.form-free-text') );
        });

        $('body').addClass('radio');

        $('.input.radio').each(function() {
            $(this).appendTo('.radio-questions');
        });

        $('.form-sub-free-text').each(function() {
           if( $(this).closest('.inputs-wrapper.alignleft.span50').length ) {
               $(this).appendTo( $(this).prev('.input') );
           }
        });
    }

    /**
     * Social Blog Search Filters
     */
    $('.magnifier:not(input)').click(function () {
        var if_empty = $('input.magnifier').val();

        if( if_empty.length > 2 ) {
            $(this).closest('.search-form').submit();
        }
    });

    $('.selected-cat').click(function () {
        $('#select-cat').slideToggle();
    });

    $('#select-cat').mouseleave(function () {
        $(this).slideUp();
    });

    $('.free-search .search-submit').prop('disabled', true);
    $('input.magnifier').on('input blur', function() {
        var this_val = $(this).val();

        if( this_val.length > 2 ) {
            $('.free-search .search-submit').prop('disabled', false);
        }

        else {
            $('.free-search .search-submit').prop('disabled', true);
        }
    });

    $('.mobile-search-bar').click(function () {
        var current_height = $('.search-bar').css('height');
        current_height = parseInt(current_height.replace('px', ''));

        if( current_height < 100 ) {
            $('.search-bar').show();
            $('.search-bar').stop().animate({ 'height': '220px', 'opacity': '1' }, 300);
        }

        else {
            $('.search-bar').stop().animate({ 'height': '0px', 'opacity': '0' }, 300);
            $('#select-cat').hide();

            setTimeout(function() {

            }, 600)
        }
    });

    /**
     * Page Social Blog
     */
    $('.featured-carousel').owlCarousel({
        autoplay: true,
        loop: true,
        paginationSpeed: 5000,
        stopOnHover : false,
        dots: true,
        items: 1
    });

    /**
     * PAGE-SUPPLIERS-YNYCC
     */
    $('input#if_tlc').next('ul').find('li').on('click', function() {
        var if_tlc = $(this).text();

        if( if_tlc == 'Yes' ) {
            $('input#tlc_number').fadeIn();
            $('input#tlc_number').addClass('error');
        }
        else {
            $('input#tlc_number').fadeOut();
        }

        $('#tlc_number').on('input blur', function() {
            var $this_val = $(this).val();
            if( $this_val.length > 5 ) {
                $(this).removeClass('error');
            }
            else {
                $(this).addClass('error');
            }
        });
    });

    if($(".page-template-page-suppliers-multiple").length > 0){
        $(".page-template-page-suppliers-multiple  .input .select span input#if_tlc").attr('readonly', true).unwrap();
    }

    /**
     * SCHEMA TAGS
     */
    if( $(window).width() > 800 ) {
        $('#nav-side .menu li > a').each(function () {
            if( $(this).attr('href') !== '#' && $(this).attr('href') !== '' ) {
                $(this).attr('itemprop', 'significantLink');
            }
        });
    }

    else {
        $('#menu-mobile li > a').each(function () {
            if( $(this).attr('href') !== '#' && $(this).attr('href') !== '' ) {
                $(this).attr('itemprop', 'significantLink');
            }
        });
    }

    $('#primary-left-menu li a img').attr('itemprop', 'image');


    /**
     * MEDIA ALIGN
     */
    var $heighest = 0;

    $('#media #from-press .workarea .ul .li .text').each(function() {
        if( $(this).height() > $heighest ) {
            $heighest = $(this).height();
        }
    });

    $('#media #from-press .workarea .ul .li .text').each(function() {
        $(this).height( $heighest );
    });

    /**
     * EXPLORER DETECTION
     */
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('body').addClass('explorer_any');
    }

    /**
    * MOBILE DETECTION
    *
    */
    var isMobile = false; //initiate as false
    var isIOS = false;

    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {

        isMobile = true;
        $('body').addClass('mobile');

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            isIOS = true;
            $('body').addClass('ios');
        }
    }

    var map = [];

    var carouselRTL = false;
    if ($('body').hasClass('rtl'))
        carouselRTL = true;
	else
	    carouselRTL = false;

    setTimeout(function () {
        $('.loader-screen').fadeOut();
    }, 1000);

	/**
	 * Hash links scroll
	 */
	$(document).on('click', 'a.anchor', function (e) {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			e.preventDefault();
			var target = window.location.hash;
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - 70
				}, 300);
				//return false;
			}
		}
	});

	/**
	 * ANCHOR SCROLL ANIMATION
	 *
	 */
	if($('a.anchor-scroll').length || $('[data-href]').length) {
		$(document).on('click', 'a.anchor-scroll', function (e) {
			e.preventDefault();

			var aTag = $(this).attr('href'),
				offset = parseFloat($(this).data('offset') ? $(this).data('offset') : 0);

			$('html,body').stop().animate({scrollTop: $(aTag).offset().top + offset}, 'slow');
		});
	}

	var hash = window.location.hash;
	if(hash){
		setTimeout(function(){
			$('html,body').stop().animate({scrollTop: $(hash).offset().top}, 'slow');
		}, 1500);
	}

	/**
	 * FAQ scroll
	 */
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}

			return false;
		}
	};

	if($('#faq').length) {
		var question = getUrlParameter('question');

		if (question) {
			setTimeout(function() {
				$('html,body').animate({
					scrollTop: $('#question' + question).offset().top
				}, 300);
			}, 1200);
		}
	}

	$(document).on('click', 'a', function (e) {
		if($(this).parent().hasClass('anchor')){
			//e.preventDefault();

			var aTag = $(this).attr('href'),
				offset = parseFloat($(this).data('offset') ? $(this).data('offset') : 0);

			var idArr = aTag.split('#');
			var id = idArr[idArr.length-1];

			$('html,body').stop().animate({scrollTop: $('#' + id).offset().top + offset}, 'slow');
		}
	});

    /**
     * Skrollr init
     */
    var s;
    if(!isMobile) {
        var skrollrInit = function () {
            setTimeout(function () {
                s = skrollr.init({
                    smoothScrolling: false,
                    mobileDeceleration: 0.004
                    ,forceHeight: false
                });
            }, 2000);
        };
    }

    if ($(window).width() > 980 && !isMobile) {
        skrollrInit()
    }
    $(window).on('resize', function () {
        if ($(this).width() <= 980 || isMobile) {
            s = false;
        }
        else {
            skrollrInit();
        }
    });

    /**
     * Header Magic Nav Line
     */

    var $magicEl, magicLeftPos, magicNewWidth;

    /* Cache it */
    var $magicLine = $('#magic-line');
    $('#magic-nav').width($('.navigation').width());
    var currentItem = $('.navigation li.current-menu-item a, .navigation li.current-page-ancestor a');
    var lineWidth = currentItem.length ? currentItem.width() : 0;
    var lineLeft = currentItem.length ? currentItem.position().left : 0;

    $magicLine
        .width(lineWidth)
        .css('left', lineLeft)
        .data('origLeft', $magicLine.position().left)
        .data('origWidth', $magicLine.width());

    $(".navigation li").hover(function () {
        $magicEl = $(this).find('a');
        magicLeftPos = $magicEl.position().left;
        magicNewWidth = $magicEl.width();

        $magicLine.stop().show().animate({
            left: magicLeftPos,
            width: magicNewWidth,
            duration: 200
        });
    }, function () {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });
    });

    /**
     * Header btn-nav (humburger)
     */
    $(document).on('click', '#btn-nav', function(e) {
        e.preventDefault();
	    $(this).toggleClass('open');

        if ($(window).width() > 1024) {
            $('#nav-side').toggleClass('in');
        }
        else {
            $('#navigation-mobile').toggleClass('in');
            // Remove scroll from body
            $('body').toggleClass('navigation-mobile-active');
        }
    });

    $(document).on('mouseenter', '.select-language', function () {
        $(this).find('ul').slideDown();
    });
    $(document).on('mouseleave', '.select-language', function () {
        $(this).find('ul').stop().stop();
        $(this).children('ul').slideUp();
    });

	/* $(document).mouseup(function (e) {
		var container = $("#nav-side, #btn-nav");

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$('#btn-nav').removeClass('open');
			$('#nav-side').removeClass('in');
			$('#navigation-mobile').removeClass('in');
			$('body').removeClass('navigation-mobile-active');
		}
	}); */



    var verticalsInterval = null,
        verticals = $('#masthead').find('.icon'),
        verticalsLength = verticals.length,
        vertical = null,
        j = 0;

    verticals.on('mouseenter', function () {
        clearInterval(verticalsInterval);
        verticals.removeClass('hover');
        vertical = null;
        j = 0;
    }).on('mouseleave', function () {
        verticalSetInterval();
    });

    verticalSetInterval();

    function verticalSetInterval() {
        verticalsInterval = setInterval(function () {
            verticals.removeClass('hover');

            if (vertical == null || j == verticalsLength) {
                j = 0;
                vertical = verticals.first();
            }
            else {
                vertical = vertical.parents('.owl-item').next().find('.icon');
            }
            vertical.addClass('hover');


            j++;
        }, 3000);
    }

    /**
     * motion scroll vertical page
     */
    /* Handles motion effect on spesific pages with sections have class motion */
    var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3
    var isAnimate = false;
    var isStop = false;

    var verticalsMotion = function () {
        $('section.motion').on(mousewheelevt, function (e) {

            e.preventDefault();

            var section = $(this);

            if (!isAnimate && !isStop) {

                isAnimate = true;
                isStop = true;

                //console.log(isStop);

                var evt = window.event || e; //equalize event object
                evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible
                var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta; //check for detail first, because it is used by Opera and FF

                if (delta > 0) {
                    //console.log('scroll up');
                    if ($(section).prev('section.motion').length) {
                        scrollTo($(section).prev('section.motion'), 'up');
                    }
                    else {
                        scrollTo('top', 'end');
                    }
                }
                else {
                    //console.log('scroll down');
                    if ($(section).next('section.motion').length) {
                        scrollTo($(section).next('section.motion'), 'down');
                    }
                    else {
                        scrollTo('bottom', 'end');
                    }
                }

                setTimeout(function () {
                    isAnimate = false;
                    isStop = false;
                }, 1000);
            }
        });
    };

    function scrollTo(element, direction) {

        var body = $('html, body');
        if (element == 'top') {
            //console.log('top');
            var scrollto = 0;
            body.animate({scrollTop: scrollto}, 500, function() {

            });
        }
        else if (element == 'bottom') {
            var scrollto = $('footer').offset().top;
            body.animate({scrollTop: scrollto}, 500, function(){

            });
        }
        else if (element.length) {
            $('section.motion').removeClass('animate');
            element.addClass('animate');
            var scrollto = $(element).offset().top;
            body.animate({scrollTop: scrollto}, 500, function() {

            });
            element.find('li').each(function (index) {
                $(this).delay(150 * index).queue(function (next) {
                    $(this).css('opacity', '1');
                    next();
                });
            });
        }
        else {

        }
    }

    if ($(window).width() > 980) {
        verticalsMotion();
    }

    $(window).on('resize', function () {
        if ($(window).width() > 980) {
            verticalsMotion();
        }
    });

    /*------------------
     ##  OWL CAROUSEL  ##
     ------------------*/

	/**
	 * OWL CAROUSELS
	 *
	 * @type {boolean}
	 *
	 */
	if($(document).find('.carousel-inner').length) {
		var carousels = [];

		$(document).find('.carousel-inner').each(function(){

			if ($(this).find('.item').length > 1) {

				carousels.push($(this));
				var currentCarousel = carousels[carousels.length - 1],
					carouselRTL = $('body').hasClass('rtl') ? true : false,
					items = currentCarousel.data('items'),
					margin = currentCarousel.data('margin'),
					multipleRows = currentCarousel.data('multiple-rows'),
					autoplayTimeout = currentCarousel.data('autoplay-timeout'),
					slideBy = currentCarousel.data('slideby') ? currentCarousel.data('slideby') : 1,
					draggable = currentCarousel.data('draggable') ? currentCarousel.data('draggable') : true,
					center = currentCarousel.data('center') ? currentCarousel.data('center') : false,
					customDots = currentCarousel.data('custom-dots') ? currentCarousel.data('custom-dots') : false;

				if (multipleRows == true) slideBy = items;

				var owlOpts = {
					margin: margin,
					autoplay: 10000,
					mouseDrag: draggable,
					autoplayTimeout: autoplayTimeout ? autoplayTimeout : 10000,
					autoplayHoverPause: true,
					dots: true,
					dotsSpeed: 700,
					center: center,
					loop: $(this).find('.item').length > 1 ? true : false,
					rtl: carouselRTL,
					slideBy: slideBy,
					responsive: {
						0: {
							items: multipleRows == true ? 3 : 1,
							margin: margin,
							slideBy: multipleRows == true ? 3 : 1,
							mouseDrag: true
						},
						768: {
							items: items
						}
					}
				};

				if (currentCarousel.data('animate') == 'fade') {
					owlOpts.animateOut = 'fadeOut';
					owlOpts.animateIn = 'fadeIn';
				}

				if(customDots){
					owlOpts.dotsContainer = '.bullets'
				}

				if ($(window).width() < 900 || isMobile)
					items = 1;

				if (!$(this).data('width')) {
					currentCarousel.owlCarousel(owlOpts);
				}
				else if ($(window).width() < parseFloat($(this).data('width'))) {
					currentCarousel.owlCarousel(owlOpts);
				}

				if ($(this).data('mobile') == 'true' && isMobile)
					currentCarousel.owlCarousel(owlOpts);

				// Go to the previous/next item
				currentCarousel.parent('.carousel-wrapper').children('.controls')
					.on('click', '.prev', function () {
						// With optional speed parameter
						// Parameters has to be in square bracket '[]'
						$(this).parents('.carousel-wrapper').children('.owl-carousel').trigger('prev.owl.carousel', [300]);
					})
					.on('click', '.next', function () {
						$(this).parents('.carousel-wrapper').children('.owl-carousel').trigger('next.owl.carousel', [300]);
					});

				currentCarousel.parent('.carousel-wrapper').children('.bullets').on('click', '.bullet', function () {
					$(this).parents('.carousel-wrapper').children('.owl-carousel').trigger('to.owl.carousel', [$(this).index(), 300]);
				});


				if($('#tariffs').length){
					// jQuery method on
					currentCarousel.on('changed.owl.carousel',function(property){
						var current = property.page.index;

						$('#section3').find('.item-background').eq(current).fadeIn('fast');
						$('#section3').find('.item-background').eq(current).siblings('div').fadeOut('fast');
					});
				}
			}
		});
	}


    var owlHP = $("#masthead").find('.carousel');
    var owlItems = $("#masthead").find('.carousel').find('.item').length >= 5 ? 5 : $("#masthead").find('.carousel').find('.item').length;

    owlHP.owlCarousel({
        loop: false,
        nav: true,
        items: owlItems,
        rtl: carouselRTL,
        navText: []
    });

    // Custom Navigation Events
    $("#masthead").find('.carousel-arrows')
        .on('click', '.next', function () {
            owlHP.trigger('next.owl.carousel');
        })
        .on('click', '.prev', function () {
            owlHP.trigger('prev.owl.carousel');
        });

    if($("#from-press").length) {
        var owlMediaPress = $("#from-press").find('ul');
        owlMediaPress.owlCarousel({
            items: 4,
            margin: 100,
            rtl: carouselRTL
        });
    }

    if($('#city').find('.carousel').length) {
        var currentCarousel = $('#city').find('.carousel'),
            carouselRTL = $('body').hasClass('rtl') ? true : false,
            items = currentCarousel.data('items'),
            margin = currentCarousel.data('margin');

        currentCarousel.on('initialized.owl.carousel', function(property) {
	        var activeslide = $('#city').find('.owl-item.active')[1];
	        $(activeslide).addClass('super-active');
	        var height = $(activeslide).parents('.carousel').height();
	        $(activeslide).parents('.carousel').height(height);
        });

        var owlOpts = {
            margin: margin,
            autoplay: true,
            mouseDrag: true,
            autoplayTimeout: 3000,
            dots: true,
            dotsSpeed: 500,
	        autoplaySpeed: 500,
            loop: true,
            rtl: carouselRTL,
	        //center: true,
            responsive: {
                0: {
                    items: 1
                },
                680: {
                    items: items
                }
            }
        };

        if(currentCarousel.data('animate') == 'fade') {
            owlOpts.animateOut = 'fadeOut';
            owlOpts.animateIn = 'fadeIn';
        }

        if($(window).width() < 900 || isMobile)
            items = 1;

        currentCarousel.owlCarousel(owlOpts);

        // Go to the previous/next item
        currentCarousel.next('.controls').on('click', '.prev', function () {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            $(this).parent('.controls').prev('.owl-carousel').trigger('prev.owl.carousel', [500]);
        })
        .on('click', '.next', function () {
            $(this).parent('.controls').prev('.owl-carousel').trigger('next.owl.carousel', [500]);
        });

        // jQuery method on change
        currentCarousel.on('changed.owl.carousel', function(property) {
            setTimeout(function() {
                var activeslide = $('#city').find('.owl-item.active')[1];
                var index = $(activeslide).children('.item').data('index');
                $(activeslide).addClass('super-active').siblings('.owl-item').each(function(){
                    if($(this).children('.item').data('index') == index){
                        $(this).addClass('super-active');
                    }
                    else{
                        $(this).removeClass('super-active');
                    }
                });

                $(activeslide).parents('.carousel-wrapper').find('.details').children('.detail').each(function(){
                    $(this).find('.value').each(function(i){
                          if(i == index){
                              $(this).addClass('active').siblings('.value').removeClass('active');
                          }
                    });
                });
            }, 0);
        });
    }

    /**
     * SELECT COUNTRIES
     */
    var currentBlog = $('.select-country .list-countries li.active').attr('data-country');
    $('#masthead').on('click', '.select-country', function () {
        $(this).children('.countries').slideToggle('fast').toggleClass('active');
	    if (!$(this).children('.countries').hasClass('active')) {
		    $('.select-country .list-countries li').removeClass('active');
		    $('.select-country .list-countries li[data-country='+currentBlog+']').addClass('active').find('a').trigger('mouseenter');
	    }
    });

    $('#masthead').on('mouseenter mouseleave', '.country', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var code = $(this).data('cities');
        $(this).parent('li').addClass('active').siblings('li').removeClass('active');
        $('.cities.' + code).stop().fadeIn().siblings('ul').hide();
    });

    setTimeout(function(){
        $('.map').find('.pin').find('.info').perfectScrollbar({minScrollbarLength: 50});
    },0);

    /**
     * BLOG PAGE - Masonry gallery
     */
    var $msnry,
        msnryRTL = true;

    if ($('body').hasClass('rtl'))
        msnryRTL = false;


    $(document).ready(function() {
      if ($('#blocks > .workarea.social-feeds-block').length) {
          $msnry = $('#blocks > .workarea.social-feeds-block').masonry({
              // options
              itemSelector: '.item',
              originLeft: msnryRTL
          });
      }
    })



	/**
	 * FAQ PAGE
     *
     */
    if($('#faq').length) {
        $('form').on('submit', function(){
            $(this).find('.loader-wrapper').show();
        });
    }
    $(document).on('click', '.btn-open-faq', function(e) {
        e.preventDefault();
        if ($("body").hasClass("mobile")) {
            $(this).parents('li').find('.answer').toggle();   
        }
        else {
            $(this).parents('li').find('.answer').slideToggle('fast');
        }
        $(this).parents('li').toggleClass( "active" );
    });

    /**
     * Dropdowns
     */
    $(document).on('click', '.sort-dropdown', function (e) {
        e.preventDefault();
        var dropdownID = $(this).data('dropdown-id');
        $(this).find('#' + dropdownID).slideToggle();
    });

    if($('#careers').length){
        $('#countries').find('li').each(function(){
            var defaultFilter = $(this).data('default-filter');

            if(defaultFilter == 'yes'){
                var item = $(this).data('filter-country');
                var item_name = $(this).html();
                $(this).parents('.countries').find('.country-selected-text').html(item_name);
                $('[data-location]').show();
                if (item != 'all') {
                    $('[data-location]').each(function () {
                        if ($(this).data('location') != item) {
                            $(this).hide();

                            setTimeout(function () {
                                s.refresh();
                            }, 100);
                        }
                    });
                }
            }
        });

        // Display image without saving it
        function readURL_Careers(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).prev('span').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('input[type="file"]').change(function () {
            readURL_Careers(this);
        });

	    $.ajax({
		    type: 'POST',
		    url: ajax_url,
		    data: {action: 'getcareers'},
		    dataType: 'json',
		    success: function (response) {

			    $('#jobs-wrapper').append(response.careers).children('.loader-wrapper').hide();
                $('#departments').append(response.departments).children('.loader-wrapper').hide();
			    $('.jobs').children('.filter').children('.open-jobs').children('.num').html(response.count);

			    $('#countries').find('li').each(function(){
				    if($(this).data('default-filter') == 'yes'){
					    $(this).trigger('click');
					    $(this).parent('#countries').hide();
				    }
			    });

          // Show all departments and countries positions if none avail
          // for current country.
          if ( $( '.num' ).html() == "0" ) {
            filterCareers('[data-filter-department="all"]', 'department', 'country', 'departments', false, 'location');
            filterCareers('[data-filter-country="all"]', 'country', 'department', 'countries', 'location');
          }

		    }
	    });
    }

    $(document).on('click', '[data-filter]', function () {
        var data = $(this).data('filter'),
            text = $(this).text();

        $(this).parents('.sort-dropdown').find('.value-selected').text(text);

        if (data == 0) {
            $(document).find('.to-filter').show();
        }
        else {
            $(document).find('.to-filter:not(.' + data + ')').hide();
            $(document).find('.to-filter.' + data).show();
        }

        if ($('.js-masonry').length) {
            $msnry.masonry('layout');
        }

        setTimeout(function () {
            s.refresh();
        }, 300);
    });

    /**
    * Filter fetched career results
    * @param obj filters = the filters collection
    * @param str filter1 = the currently selected filter
    * @param str filter2 = the filter already selected
    * @param str plural  = a plural name for filter1
    * @param str filter1/2AltName = an alternative name for filter1/2 if there is a need to declare one (due to how the careers page was constructed)
    */
    function filterCareers(filters, filter1, filter2, plural, filter1AltName, filter2AltName ) {

      // A dumb implementation of default function parameter values, thanks to IE:
      filter1AltName = filter1AltName === undefined ? false : filter1AltName;
      filter2AltName = filter2AltName === undefined ? false : filter2AltName;

      var newFilter1     = $(filters).data('filter-' + filter1),
          filter1AltName = filter1AltName ? filter1AltName : filter1,
          filter2AltName = filter2AltName ? filter2AltName : filter2,
          noCareerRes    = "<li id='no-career-results' style='display: list-item; width: 25%; height: 476.25px'></li>";
     
      $( '.' + filter1 + '-selected').data('selected-' + filter1AltName, newFilter1);

      var selectedFilter1 = $('.'+filter1+'-selected').data('selected-'+filter1AltName),
          selectedFilter2 = $('.'+filter2+'-selected').data('selected-'+filter2AltName),
          filter1Name     = $(filters).html();
          filterTotal     = $('[data-'+filter1AltName+']').length, // Number of total careers under filter1
          i               = 0;

        $(filters).parents('.'+plural).find('.'+filter1+'-selected-text').html(filter1Name);
        
        // Reset last operation:
        $("#no-career-results").remove();
        $('[data-'+filter1AltName+']').hide();
        

        $('[data-'+filter1AltName+']').each(function () {

            if ( $(this).data(filter1AltName) == selectedFilter1 || selectedFilter1 == 'all' || $(this).data('seconddepartment') == selectedFilter1 ) {
              if ( $(this).data(filter2AltName) == selectedFilter2 || selectedFilter2 == 'all' || $(this).data('seconddepartment') == selectedFilter2 ) {
                i++;
                showCareer(this, i, filterTotal);
              }
            } else {
              injectTotalCareerResults(i);
            }

        });

        // No results, show empty box (helps to keep the page's normal height):
        if ( i == 0 ) {
          $('#jobs-wrapper #jobs').append(noCareerRes);
        }
    }

    function injectTotalCareerResults(total) {
      $('.jobs').children('.filter').children('.open-jobs').children('.num').html(total);
    }

    function showCareer(career, i, length) {
      $(career).show();

      setTimeout(function () {
          s.refresh();
      }, 100);

      injectTotalCareerResults(i);
    }

    $(document).on('click', '[data-filter-department]', function (careerInstance) {
      filterCareers(this, 'department', 'country', 'departments', false, 'location');
    });

    $('#countries').find('li').on('click', function (careerInstance) {
      filterCareers(this, 'country', 'department', 'countries', 'location');
    });

    $(document).on('click', '.promotion .btn-close', function (e) {
        e.preventDefault();
        $('.promotion').fadeOut();
    });

    /**
     * Processing forms
     */

    /* ######## SEND A CV FORM ####### */
    // Check CV upload change
    $('.send-cv').find('input[type="file"]').each(function () {
        $(this).on('change', function () {
            $(this).parent('.input.file').addClass('uploaded');
        });
    });
    // On submit, send form (ajax WP action is a hidden input
    $('#CVForm').on('submit', function (e) {
        e.preventDefault();
        $('#CVForm').children('.loader-wrapper').show();
        if (validateForm($(this))) {
            $.ajax({
                type: 'POST',
                url: ajax_url,
                data: new FormData(this),
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (response) {
                    setTimeout(function () {
                        $('#CVForm').children('.loader-wrapper').hide();
                        if (response.status == 'success') {
                            // Fade out the form and title
                            $('.send-cv').find('form').hide();
                            $('.send-cv').find('.form-title').hide();
                            $('.send-cv').find('.form-success').fadeIn();
                            $('.send-cv').find('.form-success').children('span').html(response.data.cv);

                            console.log('RESPONSE', response);

                            setTimeout(function () {
                                $('.send-cv').find('form').fadeIn().find('input[type!="submit"]').val('').parent('.input').removeClass('uploaded');
                                $('.send-cv').find('.form-title').fadeIn();
                                $('.send-cv').find('.form-success').hide();
                            }, 3000);

                        }
                        else {
                            console.log(response.status);
                            console.log(response.message);
                        }
                    }, 1000);
                }
            });
        }
        else{
            $('#CVForm').children('.loader-wrapper').hide();
        }
    });

    /* ######## CONTACT FORM ####### */
    // Dropdowns
    $('section').on('click', '.select', function (e) {
        e.stopPropagation();
        e.preventDefault();
        
        if (!$(this).hasClass('active'))
            $('section').find('.select.active').removeClass('active').children('ul').slideUp('fast');

        $(this).children('ul').slideToggle('fast');
        $(this).toggleClass('active');
    });

    $('section').on('focus', '.select', function () {
        $(this).addClass('focus');
    }).on('focusout', '.select', function () {
        $(this).removeClass('focus');
        // $(this).find('input').attr('readonly', true);
    });

    if ($('.select').length) {
        // Close dropdown options list on escape
        $(document).keyup(function(e) {
            if (e.keyCode === 27) {
                $('.select.active').removeClass('active').children('ul').slideUp('fast');
            }
        });

        $(document).mouseup(function (e) {
            var container = $(".select");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.removeClass('active').children('ul').slideUp('fast');
            }
        });
    }

    if ($('.focus').length) {
        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13' || keycode == '40') {
                event.preventDefault();
                // ENTER
                $('.focus').children('ul').slideToggle('fast');
                $('.focus').toggleClass('active');
            }

        });
    }


    // Auto Complete - START

    $('.autocomplete').on('input change', function() {
        var ac_input = $(this).val().toLowerCase();
        var ac_length = ac_input.length;

        $('.autocomplete_list li').each(function() {

            var this_text = $(this).find('a').text().toLowerCase();

            // If no input, show all
            if( ! ac_input ) {
                $(this).removeClass('hide_from_list');
            }

            // Auto complete
            if( this_text.indexOf(ac_input) == -1 ) {
                $(this).addClass('hide_from_list');
            }
            else {
                $(this).removeClass('hide_from_list');
                $('.autocomplete_list').show();
            }

        });
    });

    // Auto Complete - END

    /*$(document).mouseup(function (e) {
        var container = $(".select-country");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.find('.countries').slideUp('fast');
        }
    });*/

    $('form').find('.select').on('click', 'li', function () {
        var val = $(this).data('select-value'),
            that = $(this);

        if (val.toLowerCase() == 'other') {
            $(this).parent('ul').prevAll('input').val('').focus().removeAttr('readonly');
            $(this).parent('ul').prevAll('input').addClass('highlight');

            setTimeout(function () {
                that.parent('ul').prevAll('input').removeClass('highlight');
            }, 200)
        }
        if ($(this).parent('ul').prevAll('input').val == "") {
            $(this).parent('ul').prevAll('input').attr('data-empty', '');
        }
        else {
            $(this).parent('ul').prevAll('input').val(val);
            $(this).parent('ul').prevAll('input').attr('data-empty', 'false');
        }
        
        $(this).closest('.input-wrapper').removeClass('error');
    });

    // Display image without saving it
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).next('.photo-container').addClass('active');
                $(input).next('.photo-container').children('.photo').css('background-image', 'url(' + e.target.result + ')');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    if($('.form-friends').length){
        $('.wpcf7-submit').on('click', function (e) {
            jQuery('form .form-friends .loader-wrapper').show();
            if(!(validateForm($('.form-friends').parent('form')))){
                // e.preventDefault();
            }
        });
    }

    if($('.workarea .wpcf7-form').length){
        $('.workarea .wpcf7-form .wpcf7-submit').on('click', function (e) {
            jQuery('form .suppliers-form-multiple .loader-wrapper').show();
            if(!(validateForm($('.suppliers-form-multiple').parent('form')))){
                e.preventDefault();
            }

            if ( ( validateForm( $('.form-friends').parent('form') ) ) ) {

              // Prevent GTM event submission if Google reCaptcha is present and returns a negative result
              if ( typeof grecaptcha == 'object' && grecaptcha.getResponse() == '' ) {
                return;
              }

              gettEventPushGtm();
            }
        });
    }

    if ( $('#suppliers-form').length || $( '#suppliers-form-mini' )) {
        $('#supplier-form-form').on('submit', function(e) {
            e.preventDefault();

            $('#supplier-form-form').children('.loader-wrapper').show();
            $( '.submit' ).addClass( 'loading' );

            if (validateForm($(this))) {

              if ( $( '.field-error' ).length ) {
                $('.field-error').hide()
              }

              if ( $( '.input-error' ).length ) {
                $( '[name]' ).removeClass( 'input-error' );
              }

                $.ajax({
                    type: 'POST',
                    url: ajax_url,
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);

                        if ( response.status == 'failure' && $( '.field-error' ).length ) {

                          $(response.message).each(function(i,v) {

                            let fieldName         = Object.getOwnPropertyNames(v)[0],
                                fieldElement      = $( '[name^="'+fieldName+'"]' );

                            fieldElement.nextAll('.field-error').show();
                            fieldElement.addClass( 'input-error' );
                            $('#supplier-form-form').children('.loader-wrapper').hide();
                            $( '.submit' ).removeClass( 'loading' );
                          })

                          return false;
                        }

                        setTimeout(function () {
                            $('#supplier-form-form').children('.loader-wrapper').hide();
                            $( '.submit' ).removeClass( 'loading' );
                            if (response.status == 'success') {
                                $('#supplier-form-form').css('opacity', 0);
                                $( '.confirmation-popup' ).fadeIn();
                                $('#success-message').css({
	                                opacity: 1,
	                                zIndex: 1
                                });

                                if ( response.hasOwnProperty('redirect_to') ) {

                                  setTimeout( function(){

                                    window.location = response.redirect_to;
                                  }, 5000);
                                }

                                if ( typeof gettEventPushGtm == 'function' ) {
                                  gettEventPushGtm();
                                }
                            }
                            else {
                                $( '.submit' ).removeClass( 'loading' );

                                $(response.message).each(function(i,v) {
                                    $('[name="'+Object.keys(v)[0]+'"]').closest('.input-wrapper').addClass('error');
                                });

                                console.log(response.status);
                                console.log(response.message);
                            }
                        }, 1000);
                    }
                }).done(function (response) {
                    console.log(response);
                });
            } else {
                $('#supplier-form-form').children('.loader-wrapper').hide();
                $( '.submit' ).removeClass( 'loading' );
            }
        });
    }

    // On submit, send form (ajax WP action is a hidden input
    $('#contactForm').on('submit', function (e) {
        e.preventDefault();

        $('#contactForm').children('.loader-wrapper').show();

        if (validateForm($(this))) {
            $.ajax({
                type: 'POST',
                url: ajax_url,
                data: new FormData(this),
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    setTimeout(function () {
                        $('#contactForm').children('.loader-wrapper').hide();
                        if (response.status == 'success') {
                            $('#contactForm').hide();
                            $('#contact').find('.form-title').hide();
                            $('#success-message').fadeIn();

                            /*
                             setTimeout(function () {
                             $('#success-message').hide();
                             $('#contactForm').fadeIn();
                             $('#contact').find('.form-title').fadeIn();
                             }, 5000);
                             */
                        }
                        else {
                            console.log(response.status);
                            console.log(response.message);
                        }
                    }, 1000);
                }
            }).done(function (response) {
                console.log(response);
            });
        } else {
            $('#contactForm').children('.loader-wrapper').hide();
        }
    });

    /**
     * CONTACT PAGE - GOOGLE MAPS
     */
    var i_renderMap = 0;
    $('.acf-map').each(function () {
        render_map($(this), i_renderMap);
        i_renderMap++;
    });

    $('#contact').children('.our-offices').find('.office-list').children('li').on('click', 'a', function (e) {
        e.preventDefault();

        $(this).parent('li').addClass('current').siblings('li').removeClass('current');

        var count = $(this).parent('li').data('index');
        $('#contact').find('.map-wrapper[data-index="' + count + '"]').addClass('current').siblings('.map-wrapper').removeClass('current');
	    //$('#contact').find('.map-wrapper[data-index="' + count + '"]').addClass('show').siblings('.map-wrapper').removeClass('show');
       $('#contact').find('.map-wrapper[data-index="' + count + '"]').siblings('.map-wrapper').hide();

        $('#contact').find('.map-wrapper[data-index="' + count + '"]').fadeIn('normal', function () {
            google.maps.event.trigger(this.querySelector('.acf-map'), 'resize');
            center_map(map[count-1]);
        });
    });

    /**
     * Our Offices Magic Nav Line
     */

    var $magicEl_offices, magicLeftPos_offices, magicNewWidth_offices;

    /* Cache it */
    if ($('#magic-line-offices').length) {
        var $magicLine_offices = $('#magic-line-offices');
        $('#magic-nav-offices').width($('#magic-nav-offices').prev('ul').width());

        $magicLine_offices
            .width($('#magic-nav-offices').prev('ul').children('li.current').children('a').width())
            .css('left', $('#magic-nav-offices').prev('ul').children('li.current').children('a').position().left)
            .data('origLeft', $magicLine_offices.position().left)
            .data('origWidth', $magicLine_offices.width());

        $('#magic-nav-offices').prev('ul').children('li').hover(function () {
            $magicEl_offices = $(this).find('a');
            magicLeftPos_offices = $magicEl_offices.position().left;
            magicNewWidth_offices = $magicEl_offices.width();

            $magicLine_offices.stop().show().animate({
                left: magicLeftPos_offices,
                width: magicNewWidth_offices,
                duration: 200
            });
        }, function () {
            $magicLine_offices.stop().animate({
                left: $magicLine_offices.data("origLeft"),
                width: $magicLine_offices.data("origWidth")
            });
        });
    }

    /**
     * open submenu
     */
    var verticalSubmenu = false;
    $('#masthead').find('nav')
        .on('mouseenter', 'li', function () {
            // Reset, hide everything
            verticalSubmenu = false;
            $(this).siblings('li').find('.submenu').slideUp('fast');

            // Show the right submenu if there is one
            if($(this).hasClass('menu-item-has-children')) {
                $(this).find('.submenu').slideDown();
                verticalSubmenu = true;
            }
        })
        .on('mouseleave', 'li.menu-item-has-children', function () {
            // If not on the submenu, hide it
            if(!verticalSubmenu)
                $(this).find('.submenu').slideUp();
        })
        .on('mouseenter', 'li .submenu', function (e) {
            // If on submenu, put variable to true, so it doesn't go up
            e.stopImmediatePropagation();
            e.stopPropagation();
            verticalSubmenu = true;
        })
        .on('mouseleave', 'li .submenu', function (e) {
            // When we leave the submenu, put variable to false, so it can go up
            e.stopImmediatePropagation();
            e.stopPropagation();

            verticalSubmenu = false;

            verticalSubmenuFunc();
        });

    var verticalSubmenuFunc = function(){
        if(!verticalSubmenu){
            $('#masthead').find('nav').find('li').siblings('li').find('.submenu').slideUp('fast');
        }
    };

    /**
     * GOOGLE MAPS
     *
     * render_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type    function
     *  @date    8/11/2013
     *  @since    4.3.0
     *
     *  @param    $el (jQuery element)
     *  @return    n/a
     */

    function render_map($el, i) {

        // var
        var $markers = $el.find('.marker');

        // vars
        var args = {
            zoom: 16,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            disableDoubleClickZoom: true
        };

        // create map
        //var map = new google.maps.Map( $el[0], args);
        map[i] = (function (o, a) {
            var m = new google.maps.Map(o, a);
            google.maps.event.addListener(o, 'toggle', function () {
                var c = m.getCenter();
                google.maps.event.trigger(m, 'resize');
                if (!m.get('centered')) {
                    m.setCenter(c);
                    m.set('centered', 1)
                }

            });
            return m;
        })($el[0], args);

        // add a markers reference
        map[i].markers = [];

        // add markers
        $markers.each(function () {

            add_marker($(this), map[i]);

        });

        map[i].set('styles', [
            {
                featureType: 'all',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#efefef'}
                ]
            }, {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#cccccc'}
                ]
            }, {
                featureType: 'road',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#ffffff'}
                ]
            }, {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [
                    {color: '#e7e7e7'}
                ]
            }, {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [
                    {color: '#b0b0b0'}
                ]
            }, {
                featureType: 'road.highway',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#b5b5b5'}
                ]
            }, {
                featureType: 'road',
                elementType: 'labels.icon',
                stylers: [
                    {saturation: -100},
                    {lightness: 99},
                    {gamma: 0.01}
                ]
            }, {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [
                    {visibility: 'off'}
                ]
            }, {
                featureType: 'poi',
                elementType: 'labels.icon',
                stylers: [
                    {visibility: 'off'}
                ]
            }, {
                featureType: 'transit.station',
                elementType: 'labels',
                stylers: [
                    {visibility: 'off'}
                ]
            }
        ]);

        // center map
        center_map(map[i]);

    }

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$marker (jQuery element)
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function add_marker($marker, map) {

        // var
        var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

        // create marker
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: template_directory + '/images/marker.svg'
        });

        // add to array
        map.markers.push(marker);

        // if marker contains HTML, add it to an infoWindow
        if ($marker.html()) {
            // create info window
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function () {

                infowindow.open(map, marker);

            });
        }

    }

    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function center_map(map) {

        // vars
        var bounds = new google.maps.LatLngBounds();

        var latlng;

        // loop through all markers and create bounds
        $.each(map.markers, function (j, marker) {

            latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

            bounds.extend(latlng);

        });

        latlng = new google.maps.LatLng(map.markers[0].position.lat(), map.markers[0].position.lng());
        // only 1 marker?
        if (map.markers.length == 1) {
            // set center of map
            map.setCenter(latlng);
            map.setZoom(16);
        }
        else {
            // fit to bounds
            map.fitBounds(bounds);
        }

    }


    /**--------------
     * ## HOMEPAGE ##
     --------------*/

    $(document).on('click', '.btn-scroll-down', function (e) {
        e.preventDefault();
        $('html,body').animate({scrollTop:$(this).parents('section').next().offset().top  - $( '#masthead' ).height() });
    });
    $(document).on('mouseenter', '.map .pin-circle', function () {
        $(this).parents('.pin').addClass('pin-hover');
        $(this).parents('.pin').find('.info').perfectScrollbar({minScrollbarLength: 50});
    });
    $(document).on('mouseleave', '.map .pin', function () {
        var el = $(this);
        el.removeClass('pin-hover');
    });

    if ($('.map').length) {
        $(window).on('scroll', function () {
            var $section5 = $('.map').parents('section').offset().top - $(window).scrollTop() - 300;
            if ($section5 <= $('.map').parents('section').height()) {
                var i = 0;
                $('.map li').each(function () {
                    var el = $(this);
                    setTimeout(function () {
                        el.addClass('on');
                    }, i);
                    i = parseInt(i + 100);
                });
            }
        });
    }

    /*$('#homepage .section1-video').videoBG({
        zIndex: -1,
        scale: true,
        poster: template_directory + '/images/section1-poster.jpg',
        mp4: template_directory + '/videos/video.mp4',
        webm: template_directory + '/videos/video.webm',
        ogv: template_directory + '/videos/video.mp4'
    });*/

	if($('.map').length) {
		var mapInterval = null,
			pins = $('.map').find('.pin'),
			pinsLength = pins.length,
			pin = null,
			pinsCounter = 0;

		pins.on('mouseenter', function () {
			clearInterval(mapInterval);
			pins.removeClass('pin-hover');
			pin = null;
			pinsCounter = 0;
		}).on('mouseleave', function () {
			//$('.pin-hover').fadeOut(function () {
			pinSetInterval();
			//});
		});

		pinSetInterval();

		function pinSetInterval() {

			mapInterval = setInterval(function () {
				pins.removeClass('pin-hover');
				if (pin == null || pinsCounter == pinsLength) {
					pinsCounter = 0;
					pin = pins.first();
				}
				else {
					pin = pin.next('.pin');
				}

				pin.addClass('pin-hover');
				pin.find('.info').perfectScrollbar({minScrollbarLength: 50});

				pinsCounter++;
			}, 3000);
		}
	}

    setTimeout(function(){
	    if ($('#ticker').length) {
		    $('#ticker').vTicker({
			    speed: 400,
			    pause: 2000,
			    mousePause: false
		    });
	    }
    }, 500);

    /**
     * Vertical page
     */
    $('#quotes').owlCarousel({
        items: 1,
        autoPlay: 10000,
        mouseDrag: true,
        dots: true,
        loop: true,

        // rtl: carouselRTL
    });

    /**
     * Business page
     */
    if( $('body').hasClass('page-template-page-drivers-form-new') && $(window).width() <= 800 ) {
        $('.item.alignleft.span33:last-child').wrap('<div class="items cf last"></div>');
        $('.items.cf.last').insertAfter('.apps.cf');
    }

    /**
     * Business page
     */
    $(document).on('click', '.btn-play.btn-play-video', function (e) {
        e.preventDefault();
        $('.lb.video').fadeIn(function () {
            playVideo();
        });
    });

    // On submit, send form (ajax WP action is a hidden input
    $('#business-form').on('submit', function (e) {
        e.preventDefault();

        $('#business-form').children('.loader-wrapper').show();
        $('.submit').addClass('loading');

        if (validateForm($(this))) {
            $.ajax({
                type: 'POST',
                url: ajax_url,
                data: new FormData(this),
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    setTimeout(function () {
                        if (response.status == 'success') {
                            //$('#business-form').hide();
                            //$('.bottom').hide();
                            //$('#business').find('.form').find('.form-title').hide();
                            $('.form-success').fadeIn();
                            $( '.confirmation-popup' ).fadeIn();
                            $( '.submit' ).removeClass( 'loading' );

                            if ( $( '.google-report-conversion-btn' ).length ) {
                              $( '.google-report-conversion-btn' ).trigger( 'click' );
                            }

                            if ( $( '.gett-4-b-form-x' ).length ) {
                              $( '.gett-4-b-form-x' ).hide();
                            }

                            if ( $( '.form-notes' ).length ) {
                              $( '.form-notes' ).hide();
                            }

                            if ( typeof gettEventPushGtm == 'function' ) {
                              gettEventPushGtm();
                            }
                        }
                        else {
                            console.log(response);
                            console.log(response.message);
                        }
                    }, 1000);
                }
            }).done(function (response) {
                // YAY !
                $('#business-form').children('.loader-wrapper').hide();
                $( '.submit' ).removeClass( 'loading' );
            });
        } else {
            $('#business-form').children('.loader-wrapper').hide();
            $( '.submit' ).removeClass( 'loading' );
        }
    });

    /**
     * Media
     */
    var mediaMenu = '',
        mediaMenu_i = 0,
        mediaMenuLength = $('#media').find('h3').length;

    $('#media').find('h3').each(function(){
        var sectionId = $(this).parents('section').attr('id');

        mediaMenu += '<li><a class="anchor-scroll" href="#'+ sectionId +'">'+ $(this).text() +'</a></li>';

        mediaMenu_i++;

        if(mediaMenu_i == mediaMenuLength)
            $('#media').find('.media-nav').children('ul').append(mediaMenu);
    });


	var mediaCarousels = [];

	$('#media').find('.carousel-item-4').each(function() {
		mediaCarousels.push($(this));
		var currentCarousel = mediaCarousels[mediaCarousels.length - 1];

		var opts = {
			rtl: carouselRTL,
			autoplay: false,
			items: 1,
			margin: 10,
			mouseDrag: true,
			autoplayTimeout: 3000,
			dots: true,
			dotsSpeed: 700,
			responsive: {
				768: {
					items: 2
				},
				980: {
					items: 4
				}
			}
		};


		if(currentCarousel.find('.li').length > 2) {
			opts.loop = true;
		}
		if(currentCarousel.find('.li').length > 4) {
			opts.loop = true;
		}

		currentCarousel.owlCarousel(opts);
	});

    $('#media').find('.carousel').each(function() {
	    mediaCarousels.push($(this));
	    var currentCarousel = mediaCarousels[mediaCarousels.length - 1];

	    var opts = {
		    rtl: carouselRTL,
		    autoplay: true,
		    items: 1,
		    mouseDrag: true,
		    autoplayTimeout: 3000,
		    dots: true,
		    dotsSpeed: 700
	    };

	    if(currentCarousel.find('.li').length > 1)
		    opts.loop = true;

	    currentCarousel.owlCarousel(opts);
    });
    $('#media').find('.carousel-item-rows').each(function(){
	    mediaCarousels.push($(this));
	    var currentCarousel = mediaCarousels[mediaCarousels.length - 1];

	    var opts = {
	        rtl: carouselRTL,
	        autoplay: false,
	        items: 1,
	        mouseDrag: true,
	        autoplayTimeout: 3000,
	        dots: true,
	        dotsSpeed: 700
	    };

	    if(currentCarousel.find('.li').length > 1) {
		    //opts.loop = true;
	    }

	    currentCarousel.owlCarousel(opts);
    });

    // Go to the previous/next item
    $('#media .controls').on('click', '.prev', function () {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            $(this).parent('.controls').prev('.owl-carousel').trigger('prev.owl.carousel', [300]);
        })
        .on('click', '.next', function () {
            $(this).parent('.controls').prev('.owl-carousel').trigger('next.owl.carousel', [300]);
        });

    /**
     * City page
     */
    $('#city').on('input', 'input[name="searchCity"]', function(){
        var val = $(this).val().toLowerCase();

        if(val.length < 1) {
            $('#city').find('.city-list').children('li').removeClass('active');
            return;
        }

        $('#city').find('.city-list').children('li').each(function(){
            var city = $(this).data('city').toLowerCase();

            if(city.indexOf(val) > -1)
                $(this).addClass('active');
            else
                $(this).removeClass('active');
        });
    });

    var window_w = $(window).width();
    $(window).on('resize', function(){
        window_w = $(window).width();
    });

    /**
     * Videos
     */
    $(document).on('click', '.lb .btn-close', function (e) {
        e.preventDefault();
        $(this).fadeOut(function () {
            stopVideo();
        });
        stopVideo();
        $('.lb.video').fadeOut();
    });
    $(document).on('click', '.lb', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).fadeOut(function () {
            stopVideo();
        });
    });

    $(document).on('click', '[data-video]', function(e) {
        e.preventDefault();
        var videoID = $(this).data('video');
        $('.lb.video').fadeIn(function () {
            playVideo(videoID);
        });
    });


    /**
     * MOBILE NAVIGATION
     */
    $('#navigation-mobile').on('click', 'li.menu-item-has-children > a', function(e) {
        e.preventDefault();

        if( $(this).next('.not-hidden').length ) {
            $(this).next('ul').removeClass('not-hidden').slideUp();
        }
        else if( $(this).next('ul:hidden') ) {
            $(this).closest('li').find('ul').addClass('not-hidden').slideDown('fast');
        }

    }).on('click', '.selected-item', function () {
        var that = $(this);
        that.next('.select-items').slideToggle('fast');
    });

    $(document).on('click', function (e) {
		var container = $("#navigation-mobile, #btn-nav, #nav-side");
		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$('#btn-nav').removeClass('open');
            if ($(window).width() > 800) {
                $('#nav-side').removeClass('in');
            }
            else {
                $('#navigation-mobile').removeClass('in');
            }
		}
	});

    /**
     * Share links
     */
    $('.sharer').on('click', function(e) {
        e.preventDefault();

        sharerWindow($(this));
    });

    /**
     * Apply date and time pickers to matching fields
     */

     if ( $( '.inputs-wrapper.cf' ).attr('datepicker-localization') == 'he' ) {
       $.datepicker.setDefaults( $.datepicker.regional.he );
     }

     if ( $('[data-date-field="true"]', '.inputs-wrapper.cf').length > 0 ) {

       $('[data-date-field="true"]', '.inputs-wrapper.cf').each(function() {
         // Determine whether to allow direct month/year selection
         let showMonthYearSelectBox = $(this).attr('data-date-field-year-month') == 'true' ? true : false;

         $(this).datepicker({
           dateFormat: 'dd/mm/yy',
           changeMonth: showMonthYearSelectBox,
           changeYear: showMonthYearSelectBox,
           yearRange: "c-120:c+20",
         })

       })
     }

     if ( $('[data-time-field="true"]', '.inputs-wrapper.cf').length > 0 ) {
       $('[data-time-field="true"]', '.inputs-wrapper.cf').timepicker({ 'timeFormat': 'H:i' });
       $('[data-time-field="true"]', '.inputs-wrapper.cf').timepicker('option', 'step', 5);
       $('[data-time-field="true"]', '.inputs-wrapper.cf').timepicker('option', 'minTime', '08:00');
       $('[data-time-field="true"]', '.inputs-wrapper.cf').timepicker('option', 'show2400', true);
       $('[data-time-field="true"]', '.inputs-wrapper.cf').timepicker('option', 'orientation', 'rb');
     }




//----------------------------------------
// GETT FOR BUSINESS
//----------------------------------------

    if (
      $('body').hasClass('page-template-page-gett-for-business')
      || $( 'body' ).hasClass( 'page-template-page-gett-delivery-for-business' )
      || $( 'body' ).hasClass( 'page-template-page-gett-for-business-ot' )
      || $( 'body' ).hasClass( 'page-template-page-gett-delivery-for-business-shopping-il' )
      || $( 'body' ).hasClass( 'page-template-page-one-transport' )
      || $( 'body' ).hasClass( 'page-template-page-gett-for-business-ru' )
      || $( 'body' ).hasClass( 'page-template-page-gett-for-business-upper-form' )
      || $( 'body' ).hasClass( 'page-template-page-gett-for-business-front-page' )
      || $( 'body' ).hasClass( 'page-template-page-2018-gett-delivery-for-business' )
      || $( 'body' ).hasClass( 'page-template-page-gett-delivery-private-users' )
      || $( 'body' ).hasClass( 'page-template-page-vertical-champagne' )
    ){

        var header = $('#masthead');

        $(window).on('scroll', function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > 250) {
                header.addClass('scrolled');
            } else {
                header.removeClass('scrolled');
            }
        });

        var descPointTitle = $('.gett-4-b-desc-point-title');
        var descPointText = $('.gett-4-b-desc-point-text');

        descPointTitle.click(function() {
            descPointTitle.removeClass('active');
            $(this).addClass('active');

            descPointText.removeClass('shown');
            descPointText.eq( $(this).index() ).addClass('shown');
        });


    }


    // Scrolling Animation
    $( 'a.scroll[href*="#"]' ).click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top  - $( '#masthead' ).height() }, 500);
    });
});

/**--------------------
 * ## YOUTUBE VIDEOS ##
 --------------------*/
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('gett-video', {
        videoId: jQuery('#business').find('[data-video]').data('video'),
        autoplay: false,
        controls: 0
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        setTimeout(stopVideo, 6000);
        done = true;
    }
}
function stopVideo() {
    player.stopVideo();
}
function playVideo(videoId) {
    player.loadVideoById(videoId);
    player.playVideo();
}

/*******************
 ** SOCIAL SHARER **
 ******************/

function sharerWindow(_this) {
    var width = 500;
    var height = 300;
    var leftPosition, topPosition;
    //Allow for borders.
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    //Allow for title and status bars.
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
    var u = _this.attr('href');

    window.open(u, 'sharer', windowFeatures);
    return false;
}
