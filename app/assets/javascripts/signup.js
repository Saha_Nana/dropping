$(document).ready(function () {

    function toggleSecondaryInput(mainSelectSelector, secondaryBlockSelector) {
        var mainSelect = $(mainSelectSelector);
        var secondaryBlock = $(secondaryBlockSelector);

        mainSelect.on('change', function () {
            if ($('option:selected', this).val() === 'Other') {
                mainSelect.removeAttr('required');
                secondaryBlock.removeClass('hidden');
                secondaryBlock.find('input').attr('required', true);
            } else {
                mainSelect.attr('required', true);
                secondaryBlock.addClass('hidden');
                secondaryBlock.find('input').removeAttr('required');
            }
        });
    }

    function handleCitySelection() {
        var badgeSelect = $('#gett_billing_driver_signup_form_badgeType');
        var cities = {
            'London': ['Green Badge', 'Yellow Badge'],
            'Greater Manchester': ['Manchester City Council Hackney Carriage Licence', 'Salford Hackney Carriage Licence', 'Stockport Hackney Carriage Licence', 'Other'],
            'Edinburgh': ['Edinburgh Taxi Licence', 'Other'],
            'Glasgow': ['Glasgow Taxi Licence', 'Other'],
            'Coventry': ['Coventry Hackney Carriage Licence', 'Other'],
            'Birmingham': ['Birmingham City Hackney Carriage Licence', 'Other'],
            'Leeds': ['Leeds City Hackney Carriage Licence', 'Other'],
            'Liverpool': ['Liverpool Hackney Carriage Licence', 'Other'],
            'Cardiff': ['Cardiff Hackney Carriage Licence', 'Other'],
            'Newcastle': ['Newcastle Hackney Carriage Licence', 'Other'],
            'Bournemouth': ['Bournemouth Hackney Carriage Licence', 'Other'],
            'Reading': ['Reading Hackney Carriage Licence', 'Other'],
            'Bristol': ['Bristol Hackney Carriage Licence', 'Other'],
            'York': ['York Hackney Carriage Licence', 'Other'],
            'Other': ['Other']
        };
        badgeSelect.find('option').remove();
        badgeSelect.append($('<option>', { value: '', text: 'Badge Type (Required)', selected: true }));

        var typesForSelectedCity = cities[$("#gett_billing_driver_signup_form_city").val()];

        if (!typesForSelectedCity) {
            return;
        }

        typesForSelectedCity.forEach(function (val) {
            badgeSelect.append($('<option>', { value: val, text: val }));
        });
    }

    toggleSecondaryInput('#gett_billing_driver_signup_form_city', '#cityOther');
    toggleSecondaryInput('#gett_billing_driver_signup_form_vehicleModel', '#vehicleModelOther');
    toggleSecondaryInput('#gett_billing_driver_signup_form_cabColour', '#cabColourOther');
    toggleSecondaryInput('#gett_billing_driver_signup_form_badgeType', '#badgeTypeOther');
    toggleSecondaryInput('#gett_billing_driver_signup_form_howDidYouHear', '#howDidYouHearOther');
    $("#gett_billing_driver_signup_form_city").on('change', handleCitySelection);
    handleCitySelection();

    var $form = $('form');
    $form.on('invalid.fndtn.abide', function (e) {
        if (e.namespace != 'abide.fndtn') {
            return;
        }
        var invalidFields = $(this).find('[data-invalid]');
        if (invalidFields) {
            var scrollTo = $('#' + invalidFields[0].id).offset().top;
            $('html, body').animate({ scrollTop: scrollTo }, 400);
        }
    });

    $(".date-picker").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0,
        changeMonth: true,
        changeYear: true,
    });

    // handle file upload plugin init
    $(function () {
        $('#photoOfFace, #hackneyCarriageDriverLicenseBill, #hackneyCarriageDriverLicenseBadge, #DVLADriversLicense').fileupload({
            dataType: 'json',
            autoUpload: false,
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(e.target).parents('.btn').siblings('.progress').children('.bar').css(
                    'width',
                    progress + '%'
                );
            },
            add: function (e, data) {
                var id = $(e.target).attr('id');
                $(e.target).siblings('.button-label').text('CHANGE');
                $(e.target).parents('.fileinput-button').addClass('file-selected');
                $(e.target).data('file-added', true);
                $('[name=' + id + ']').val(data.files[0].name);
                $("#submit-btn").on('trigger-upload', function (e) {
                    var invalidFields = $('#driver_form [data-invalid]').length;
                    if (invalidFields > 0) {
                        return true;
                    }
                    data.submit().complete(function (result, textStatus, jqXHR) {
                        $('#driver_form').submit();
                    });
                });
            },
            change: function (e, data) {
                var id = $(e.target).attr('id');
                $(e.target).data('file-added', true);
                $('[name=' + id + ']').val(data.files[0].name);
            }
        });
    });

    $('#driver_form').submit(function (e, element) {
        // supervise only uploadable forms
        if ($('#photoOfFace').length === 0) {
            return true;
        }

        e.preventDefault();

        var unfinishedCount =
            $('#photoOfFace').fileupload('active') +
            $('#hackneyCarriageDriverLicenseBill').fileupload('active') +
            $('#hackneyCarriageDriverLicenseBadge').fileupload('active') +
            $('#DVLADriversLicense').fileupload('active');

        if (unfinishedCount > 0) {
            return true;
        }

        var formData = $("#driver_form").serializeArray();
        formData.push({ name: 'complete', value: true });
        $.ajax({
            type: "POST",
            url: $('#driver_form').attr('action'),
            data: formData,
            dataType: "json",
            success: function (data) {
                window.location = data;
            },
            error: function (xhr, status, error) {
                console.log(xhr, status, error);

                alert('Error submitting form. Please contact support.');
            }
        });
    });

    $("#submit-btn").on('click', function () {
        $('#driver_form').trigger('validate.fndtn.abide');
        var invalidFields = $('#driver_form [data-invalid]').length;
        if (invalidFields === 0) {
            $.LoadingOverlay('show');
            $("#submit-btn").trigger('trigger-upload');
        }
    });
});
