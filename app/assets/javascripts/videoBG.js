function detectmob() {
    return ( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    );
}

function isIos()
{
    return ( navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
    );
}

jQuery(document).ready(function ($) {
	if(isIos()) {
        $("#about > #section1").css("background-attachment", "local");
    }

	if($('.section1-video').length && !detectmob()) {
		$(videoBG.container).videoBG({
			zIndex: -1,
			//scale: true,
			poster: videoBG.poster.url,
			mp4: videoBG.video_mp4,
			webm: videoBG.video_webm,
			ogv: videoBG.video_ogv
		});
	}
});
