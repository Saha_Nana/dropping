$(document).foundation({
    abide: {
        validators: {
            ukBankAcc: function (el) {
                return el.value.length === 8 && /^\d+$/.test(el.value);
            },
            ukBankSort: function (el) {
                return el.value.length === 6 && /^\d+$/.test(el.value);
            },
            ukMobile: function (el) {
                return el.value.length === 11 && el.value.substring(0, 2) === '07' && /^\d+$/.test(el.value);
            },
            onlyNumbers: function (el) {
                return el.value.length > 0 && /^\d+$/.test(el.value);
            },
            onlyLetters: function (el) {
                return el.value.length > 0 && /^[a-zA-Z]+$/.test(el.value);
            },
            referrerDriverId: function (el) {
                return el.value.length === 0 || (el.value.length > 0 && /^\d+$/.test(el.value));
            },
            badgeNumber: function (el) {
                return el.value.length === 5 && /^\d+$/.test(el.value);
            },
            fileUpload: function (el) {
                return $(el).data('file-added') === true;
            }
        }
    }
});
