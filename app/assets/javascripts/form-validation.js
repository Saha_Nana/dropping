jQuery(document).ready(function($) {
	/* ######## FORM VALIDATION PROCESS ####### */
	$('form').on('blur', 'input, textarea', function () {
		validateForm($(this));
	});

	/* ######## FORM VALIDATION PROCESS ####### */
	$('form').on('input change', 'input, textarea, select', function () {
		$(this).removeClass('error');
		$(this).removeClass('field-verified');
		$(this).closest('.input-wrapper').removeClass('error');
	});

	if( $('body').hasClass('page-template-page-contact') ) {
		$('.select.cat li').on('click', function() {
			var by_cat = $(this).attr('data-send-to');
			var if_salesforce = $(this).attr('data-sales-force');

			$('#send_by_cat').val( by_cat );
			$('#if_sales_force').val( if_salesforce );
		});
	}

	/* ######## FORM CONFIRMATION POPUP ####### */
	$( '.confirmation-popup .close' ).click(function(){
		$(this).closest('.confirmation-popup').fadeOut();
	});

	$( '.confirmation-popup' ).click(function(e) {
		if (e.target !== this)
		return;
		$('.confirmation-popup').fadeOut();
	});

	
	/* ######## FORM DESIGN LABEL TRANSITION ####### */

	// If input has value on page load, do the label transition
	$('.form-design').find(':input').each(function(){
		if( $(this).val() ) {
			$(this).attr('data-empty', 'false');
		}
	});

	$('.form-design').find('input').on('input', function (e) {
		$(e.currentTarget).attr('data-empty', !e.currentTarget.value);
	});
});

function validateForm(_this) {
	var count_errors = 0;

	// If it's a form, loop through all the inputs
	if(_this.is('form')) {
		_this.find('input, textarea, select').each(function () {
			var type = jQuery(this).attr('type'),
                required = (jQuery(this).data('required') == true || jQuery(this).attr('aria-required') == "true" ) ? true : false;

			if (perType(jQuery(this), type, required)) {
				count_errors++;
				//jQuery(this).addClass('error');
			}

			jQuery('.checkboxes').each(function() {
				if( ! jQuery(this).find('input:checked').length && jQuery(this).attr('data-required') == 'true' ) {
					jQuery(this).closest('.checkboxes').addClass('error');
				}
				else {
					jQuery(this).closest('.checkboxes').removeClass('error');
				}
			});
		});
	}
	// If it's an input
	else{
		var type = _this.attr('type'),
            required = (_this.data('required') == true || _this.attr('aria-required') == "true" ) ? true : false;

		if (perType(_this, type, required)) {
			count_errors++;
			//_this.addClass('error');
		}
	}

	if (count_errors > 0) {
		jQuery('.loader-wrapper').hide();
		jQuery('.submit').removeClass('loading');
		return false;
	}

	return true;
}

var getLanguage = (location.href).split("/");
getLanguage = (getLanguage[3]) ? getLanguage[3] : "us";

function perType(_this, type, required) {
	if ( _this.attr( 'id' ) == 'g-recaptcha-response' ) {
		return;
	}

    if( _this.attr('id') == 'phone_mobile' || _this.attr('id') == 'mobile' || _this.attr('name') == 'phone' || _this.attr('name') == 'driver-phone' || _this.attr('name') == 'friend-phone') {
		var phone_value = _this.attr('value');

		if (phone_value.match(/[a-zא-ת/!@#$%]/i) || (required && phone_value.length == 0)) {
			jQuery(_this).addClass('error');
			jQuery(_this).closest('.input-wrapper').addClass('error');
			return true;
		}else{
			jQuery(_this).addClass('field-verified');
		}
	}


    if( _this.attr('name') == 'driver-number' ){
        var driver_number = _this.attr('value');
        if (!(driver_number.match(/^[0-9 ]{1,15}$/i))) {
			jQuery(_this).addClass('error');
			jQuery(_this).closest('.input-wrapper').addClass('error');
            return true;
        }else{
            jQuery(_this).addClass('field-verified');
        }
    }

    if( _this.attr('name') == 'friend-city' ){
        var driver_city = _this.attr('value');
        if (driver_city.match(/[0-9!@#$%]{1,20}/i) || driver_city.length == 0) {
			jQuery(_this).addClass('error');
			jQuery(_this).closest('.input-wrapper').addClass('error');
            return true;
        }else{
            jQuery(_this).addClass('field-verified');
        }
    }


    if( _this.attr('name') == 'friend-city' || _this.attr('name') == 'friend-name' || _this.attr('name') == 'driver-name' ){
        var driver_city = _this.attr('value');
        if (driver_city.match(/[0-9!@#$%]{1,20}/i) || driver_city.length == 0) {
			jQuery(_this).addClass('error');
			jQuery(_this).closest('.input-wrapper').addClass('error');
            return true;
        }else{
            jQuery(_this).addClass('field-verified');
        }
    }

	if( _this.attr('id') == 'postcode' )
	{
        var phone_value = _this.attr('value');

        switch(getLanguage)
		{
			case "uk":
                if (!phone_value.match(/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/i) || phone_value.length > 9) {
					jQuery(_this).addClass('error');
					jQuery(_this).closest('.input-wrapper').addClass('error');
                    return true;
                }
			break;
			default:
                if (phone_value.match(/[a-z/!@#$%]/i)) {
					jQuery(_this).addClass('error');
					jQuery(_this).closest('.input-wrapper').addClass('error');
                    return true;
                }
			break;
		}
	}
    if( _this.attr('id') == 'tlc_number' ) {
        var first_value = _this.attr('value');

        if( first_value.length < 5 && first_value.length >= 1 ) {
            jQuery('#tlc_number').addClass('error');
            return true;
        }
    }
    if( type == 'radio' ) {
        if( _this.closest('.checkboxes').hasClass('error') ) {
            return true;
        }
    }
	if( _this.attr('id') == 'first_name' ) {
		var first_value = _this.attr('value');

		if( first_value.length < 2 ) {
			jQuery('#first_name').addClass('error');
			jQuery('#first_name').closest('.input-wrapper').addClass('error');
			return true;
		}
	}
	if( _this.attr('id') == 'last_name' ) {
		var last_value = _this.attr('value');

		if( last_value.length < 2 ) {
			jQuery('#last_name').addClass('error');
			jQuery('#last_name').closest('.input-wrapper').addClass('error');
			return true;
		}
	}
	if( _this.attr('id') == 'car_type' ) {
		var car_type = _this.attr('value');

		if( car_type.length < 2 ) {
			jQuery('#car_type').addClass('error');
			jQuery('#car_type').closest('.input-wrapper').addClass('error');
			return true;
		}
	}
	if( _this.attr('id') == 'car_color_c' ) {
		var car_color_c = _this.attr('value');

		if( car_color_c.length < 2 ) {
			jQuery('#car_color_c').addClass('error');
			jQuery('#car_color_c').closest('.input-wrapper').addClass('error');
			return true;
		}
	}
	if( jQuery('#car_type_1_c').val() == 'Is your car wheelchair accessible?*' ) {
		jQuery('#car_type_1_c').addClass('error');
		jQuery('#car_type_1_c').closest('.input-wrapper').addClass('error');
		return true;
	}

	if( _this.attr('type') == 'select' && _this.attr('data-required') == '1' ) {
		var select = _this.find('option:selected').text();
		var first_option = _this.find('option:first-child').text();

		if( select.indexOf('*') !== -1 ) {
			jQuery(_this).addClass('error');
			jQuery(_this).closest('.input-wrapper').addClass('error');
			return true;
		}
	}

	if (type == 'submit')
		return false;

	else if (type == 'text') {
		if (!validateInput(_this, required)) {
			return true; // There is an error
		}
	}
	else if (type == 'email') {
		if (!validateEmail(_this, required)) {
			return true; // There is an error
		}
	}
	else if (type == 'select') {
		if (!validateSelect(_this, required)) {
			return true; // There is an error
		}
	}
	else if (type == 'tel') {
		if (!validateTel(_this, required)) {

			return true; // There is an error
		}
	}
	else if (type == 'number') {
		if (!validateNumber(_this, required)) {

			return true; // There is an error
		}
	}
	else if (type == 'date') {
		if (!validateDate(_this, required)) {

			return true; // There is an error
		}
	}
	else if (type == 'checkbox') {
		if (!validateCheckbox(_this, required)) {

			return true; // There is an error
		}
	}
	// else if (_this.is('textarea')) {
	// 	if (!validateTextarea(_this, required)) {
	//
	// 		return true; // There is an error
	// 	}
	// }
}

function validateSelect(_this, required) {
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');
	var selected_option = _this.find("select option:selected").text();
	var trigger = _this.find('option:first-child').text();

	setTimeout(function() {
		if (required && selected_option.indexOf(trigger) > -1) {
			_this.addClass('error');
			_this.closest('.input-wrapper').addClass('error');
			return false;
		}
	}, 500);

	return true;
}

function validateInput(_this, required) {
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');

	if(_this.parent('.select')){
		setTimeout(function() {
			if (required && _this.val().length == 0) {
				_this.addClass('error');
				_this.closest('.input-wrapper').addClass('error');
				return false;
			}
		}, 500);
	}
	else{
		if (required && _this.val().length == 0) {
			_this.addClass('error');
			_this.closest('.input-wrapper').addClass('error');
			return false;
		}
	}
	return true;
}

function validateEmail(_this, required) {
	var filterEmail = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');
	if (required && _this.val().length == 0) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	else if (!filterEmail.test(_this.val())) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}

function validateTel(_this, required) {
	var filterPhone = /^\d{0,10}$/;
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');
	if (required && _this.val().length == 0) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	else if (!filterPhone.test(_this.val())) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}

function validateNumber(_this, required) {
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');
	if (required && _this.val().length == 0) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	else if (parseInt(_this.val()) == NaN) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}

function validateDate(_this, required) {
	if (required && _this.val().length == 0) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}

function validateCheckbox(_this, required) {
	if (required && !_this.is(':checked')) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}

function validateTextarea(_this, required) {
	_this.removeClass('error');
	_this.closest('.input-wrapper').removeClass('error');
	if (required && _this.val().length == 0) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	else if (_this.val().indexOf(' ') == -1) {
		_this.addClass('error');
		_this.closest('.input-wrapper').addClass('error');
		return false;
	}
	return true;
}
