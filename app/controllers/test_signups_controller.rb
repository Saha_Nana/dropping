class TestSignupsController < ApplicationController
  before_action :set_test_signup, only: [:show, :edit, :update, :destroy]

  # GET /test_signups
  # GET /test_signups.json
  def index
    @test_signups = TestSignup.all
  end

  # GET /test_signups/1
  # GET /test_signups/1.json
  def show
  end

  # GET /test_signups/new
  def new
    @test_signup = TestSignup.new
  end

  # GET /test_signups/1/edit
  def edit
  end

  # POST /test_signups
  # POST /test_signups.json
  def create
    @test_signup = TestSignup.new(test_signup_params)

    respond_to do |format|
      if @test_signup.save
        format.html { redirect_to @test_signup, notice: 'Test signup was successfully created.' }
        format.json { render :show, status: :created, location: @test_signup }
      else
        format.html { render :new }
        format.json { render json: @test_signup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_signups/1
  # PATCH/PUT /test_signups/1.json
  def update
    respond_to do |format|
      if @test_signup.update(test_signup_params)
        format.html { redirect_to @test_signup, notice: 'Test signup was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_signup }
      else
        format.html { render :edit }
        format.json { render json: @test_signup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_signups/1
  # DELETE /test_signups/1.json
  def destroy
    @test_signup.destroy
    respond_to do |format|
      format.html { redirect_to test_signups_url, notice: 'Test signup was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_signup
      @test_signup = TestSignup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_signup_params
      params.fetch(:test_signup, {})
    end
end
