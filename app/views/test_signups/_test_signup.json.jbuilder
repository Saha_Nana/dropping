json.extract! test_signup, :id, :created_at, :updated_at
json.url test_signup_url(test_signup, format: :json)
