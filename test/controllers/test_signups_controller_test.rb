require 'test_helper'

class TestSignupsControllerTest < ActionController::TestCase
  setup do
    @test_signup = test_signups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:test_signups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create test_signup" do
    assert_difference('TestSignup.count') do
      post :create, test_signup: {  }
    end

    assert_redirected_to test_signup_path(assigns(:test_signup))
  end

  test "should show test_signup" do
    get :show, id: @test_signup
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @test_signup
    assert_response :success
  end

  test "should update test_signup" do
    patch :update, id: @test_signup, test_signup: {  }
    assert_redirected_to test_signup_path(assigns(:test_signup))
  end

  test "should destroy test_signup" do
    assert_difference('TestSignup.count', -1) do
      delete :destroy, id: @test_signup
    end

    assert_redirected_to test_signups_path
  end
end
